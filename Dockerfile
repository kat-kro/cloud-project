FROM Python 3.9

RUN mkdir -p /PycharmProjects/Automation/Chrome_driver/

WORKDIR /PycharmProjects/Automation/Chrome_driver/

COPY . /PycharmProjects/Automation/Chrome_driver/

CMD ["python", "main.py"]
