from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

options = webdriver.ChromeOptions()

URL = "http://qa-assignment.oblakogroup.ru/board/:andrey_ponomarev"
driver = webdriver.Chrome(ChromeDriverManager().install())

driver.maximize_window()

try:
    
    driver.get(URL=URL)
    driver.find_element_by_id("add_new_todo").click()
    driver.find_element_by_class_name("select2-selection__arrow").click()
    driver.find_element_by_class_name("select2-results__option")
    driver.find_element_by_xpath("/html/body/span/span/span[2]/ul/li[2]").click()
    driver.find_element_by_xpath("/html/body/div[1]/div/div/form/div/p[3]/input").click()

    name_input = driver.find_element_by_id("project_text")
    name_input.clear()
    name_input.send_keys("Закрыть кабинет")
    driver.find_element_by_xpath("/html/body/div[1]/div/div/form/a[2]").click()

except Exception as ex:
    print(ex)

finally:
    driver.quit()
